# ClearScore iOS test app

ClearScore iOS test demo project for testing a custom architecture that respect **SOLID** principles, is **Protocol-Oriented** and is based on native iOS MVC pattern used the right way with (small) view controllers at the centre of the architecture. Other design patterns are included to support it like Coordinator pattern (for view controllers communication) and a very pragmatic approach of MVVM pattern with view models as value types just to pass formatted data to the views.

According to Apple’s description of the MVC pattern in the official documentation:

> *"One can merge the MVC roles played by an object, making an object, for example, fulfill both the controller and view roles—in which case, it would be called a view controller. In the same way, you can also have model-controller objects."*

So, **Model Controllers**:
- support view controllers;
- access shared resources;
- deal with the model;
- they are not rigidly defined.

> Model controllers are a layer in the pattern that fills with objects with very clear, *single responsibilities*.

### Shared model controllers:
- scheduling network requests;
- preserving the app’s state;
- interacting with storage technologies like the file system, Core Data, SQLite, etc.;
- reading device sensors like the GPS, the gyroscope or the accelerometer;
- etc.

### Dedicated model controllers:
- not shared;
- support view controllers with clear responsibilities such as:
  - data sources for table/collection views;
  - state machines for complex view controller logic.


This is a very flexible architecture as you can plug-in the components into layers according to the needs. Unit tests are included to show the flexibility of this architecture.


### TODO:
- this architecture work under a Protocol-Oriented approach and the plug-in of the components that you need in each flow, so we can use other model controllers for other shared tasks (like Caching/Persistence) and compose more complex ways to load data from network and persist it according to our needs.
- we can also include Queues and Operation to avoid hard concurrency problems and to handle all background execution tasks, i.e.: schedule, suspend and cancel network requests, etc.

> This is a very simple app that could be done with a more simple approach, but is great to show how this architecture works for production-ready apps.
