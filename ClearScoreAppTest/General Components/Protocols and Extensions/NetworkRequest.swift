//
//  NetworkRequest.swift
//  ClearScoreAppTest
//
//  Created by Victor Vargas Fariñas on 29/12/2019.
//  Copyright © 2019 NUX Factory. All rights reserved.
//

import Foundation

// MARK: - NetworkRequest

protocol NetworkRequest: AnyObject {
    associatedtype ModelType

    var url: URL { get }
    var urlRequest: URLRequest { get }
    var session: URLSession { get }
    var task: URLSessionDataTask? { get set }

    func deserialize(_ data: Data?, response: HTTPURLResponse) throws -> ModelType
}

// MARK: - NetworkRequest Helpers

extension NetworkRequest {

    var urlRequest: URLRequest { URLRequest(url: url) }
    var isTestingEnv: Bool { AppConfig.isTestingEnv }

    func execute(withCompletion completion: @escaping (Result<ModelType, NetworkError>) -> Void) {
        let completionHandler: DataCompletionHandler = { [weak self] (data, response, error) in
            guard let strongSelf = self else {
                return
            }

            DispatchQueue.global(qos: .userInitiated).async {
                let result = strongSelf.result(from: data, response: response, error: error)

                DispatchQueue.main.async {
                    completion(result.mapError { $0.asNetworkError })
                }
            }
        }

        task = isTestingEnv ? SimplifiedDataTask(url: url, completion: completionHandler) : session.dataTask(with: urlRequest, completionHandler: completionHandler)
        task?.resume()
    }

}

// MARK: - NetworkRequest Private Helpers

private extension NetworkRequest {

    func result(from data: Data?, response: URLResponse?, error: Error?) -> Result<ModelType, Error> {
        return Result {
            guard let response = response as? HTTPURLResponse else {
                throw URLError(.badServerResponse)
            }

            try response.validate()

            return try deserialize(data, response: response)
        }
    }

}

// MARK: - JSONDataRequest

protocol JSONDataRequest: NetworkRequest where ModelType: Decodable {}

// MARK: - JSONDataRequest: NetworkRequest

extension JSONDataRequest {

    func deserialize(_ data: Data?, response: HTTPURLResponse) throws -> ModelType {
        guard let data = data, response.statusCode != 404 else {
            throw URLError(.badServerResponse)
        }

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601

        do {
            return try decoder.decode(ModelType.self, from: data)
        } catch {
            throw URLError(.badServerResponse)
        }
    }

}
