//
//  URLSession.swift
//  ClearScoreAppTest
//
//  Created by Victor Vargas Fariñas on 16/01/2020.
//  Copyright © 2020 NUX Factory. All rights reserved.
//

import Foundation

extension URLSession {

    static func defaultSession() -> URLSession {
        return URLSession(configuration: .default, delegate: nil, delegateQueue: .main)
    }

}
