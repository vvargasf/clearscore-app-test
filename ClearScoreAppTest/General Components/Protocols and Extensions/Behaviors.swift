//
//  Behaviors.swift
//  ClearScoreAppTest
//
//  Created by Victor Vargas Fariñas on 29/12/2019.
//  Copyright © 2019 NUX Factory. All rights reserved.
//

import UIKit

// MARK: - ErrorResponding

protocol ErrorResponding: MainCoordinated, Networked {
    var error: Error? { get }
}

// MARK: - Fetching

protocol Fetching: Networked {
    associatedtype FetchedType: Codable

    var url: URL? { get }

    func performRequest(with url: URL, completion: @escaping (Result<FetchedType, NetworkError>) -> Void)
    func handle(result: FetchedType)
    func handle(error: Error)
}

// MARK: - Fetching Helpers

extension Fetching {

    func fetchData() {
        guard let url = url else {
            return
        }

        performRequest(with: url) { [weak self] (result: Result<FetchedType, NetworkError>) in
            do {
                self?.handle(result: try result.get())
            } catch {
                self?.handle(error: error)
            }
        }
    }

}

// MARK: - ItemDataSource

protocol ItemDataSource {
    associatedtype ModelType

    init(item: ModelType)
}

// MARK: - ItemViewController

protocol ItemViewController: Fetching, ErrorResponding {
    var item: FetchableValue<FetchedType>? { get }
}

// MARK: - ItemViewController Helpers

extension ItemViewController {

    var url: URL? {
        return item?.url
    }

    func performRequest(with url: URL, completion: @escaping (Result<FetchedType, NetworkError>) -> Void) {
        networkController?.fetchValue(for: url, withCompletion: completion)
    }

}
