//
//  UIColor.swift
//  ClearScoreAppTest
//
//  Created by Victor Vargas Fariñas on 29/12/2019.
//  Copyright © 2019 NUX Factory. All rights reserved.
//

import UIKit

extension UIColor {

    private enum Palette: CaseIterable {
        case gargoyleGas, licorice, mountainMeadow, pastelGray, seaBlue, slateGray, tigerEye, whiteSmoke, yankeesBlue

        var color: UIColor {
            switch self {
            case .gargoyleGas: return UIColor(named: "Gargoyle Gas")!
            case .licorice: return UIColor(named: "Licorice")!
            case .mountainMeadow: return UIColor(named: "Mountain Meadow")!
            case .pastelGray: return UIColor(named: "Pastel Gray")!
            case .seaBlue: return UIColor(named: "Sea Blue")!
            case .slateGray: return UIColor(named: "Slate Gray")!
            case .tigerEye: return UIColor(named: "Tiger Eye")!
            case .whiteSmoke: return UIColor(named: "White Smoke")!
            case .yankeesBlue: return UIColor(named: "Yankees Blue")!
            }
        }
    }

    static let gargoyleGas = Palette.gargoyleGas.color
    static let licorice = Palette.licorice.color
    static let mountainMeadow = Palette.mountainMeadow.color
    static let pastelGray = Palette.pastelGray.color
    static let seaBlue = Palette.seaBlue.color
    static let slateGray = Palette.slateGray.color
    static let tigerEye = Palette.tigerEye.color
    static let whiteSmoke = Palette.whiteSmoke.color
    static let yankeesBlue = Palette.yankeesBlue.color
    static let palette: [UIColor] = Palette.allCases.map { $0.color }

}
