//
//  UINavigationBar.swift
//  ClearScoreAppTest
//
//  Created by Victor Vargas Fariñas on 02/01/2020.
//  Copyright © 2020 NUX Factory. All rights reserved.
//

import UIKit

extension UINavigationBar {

    static func setCustomAppearance() {
        UINavigationBar.appearance().barTintColor = .licorice
        UINavigationBar.appearance().tintColor = .gargoyleGas
        UINavigationBar.appearance().titleTextAttributes = [
            .foregroundColor: UIColor.whiteSmoke,
            .font: UIFont(name: "CSClarity-Medium", size: 24)!
        ]
    }

}
