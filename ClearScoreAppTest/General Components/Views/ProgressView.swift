//
//  ProgressView.swift
//  ClearScoreAppTest
//
//  Created by Victor Vargas Fariñas on 04/01/2020.
//  Copyright © 2020 NUX Factory. All rights reserved.
//

import UIKit

// MARK: - ProgressView

protocol ProgressViewDelegate: class {
    func didAnimateProgress(to value: ProgressView.ProgressValue, on view: ProgressView)
    func didProgressComplete(on view: ProgressView)
}

@IBDesignable
class ProgressView: UIView {
    typealias ProgressValue = CGFloat
    typealias ProgressAnimationDuration = TimeInterval
    typealias ProgressAnimationCompletion = () -> Void

    @IBInspectable
    var value: CGFloat = 0 {
        didSet {
            if value < minValue {
                value = minValue
            }

            if value > maxValue {
                value = maxValue
            }

            shapeLayer.initialValue = initialValue
            shapeLayer.value = value
        }
    }

    @IBInspectable
    var minValue: CGFloat = 0 {
        didSet {
            shapeLayer.minValue = abs(minValue)
        }
    }

    @IBInspectable
    var maxValue: CGFloat = 100 {
        didSet {
            shapeLayer.maxValue = abs(maxValue)
        }
    }

    @IBInspectable
    var pathColor: UIColor = .black {
        didSet {
            shapeLayer.basePathColor = pathColor
        }
    }

    @IBInspectable
    var progressStartColor: UIColor = .pastelGray {
        willSet {
            shapeLayer.progressPathGradientColors = [newValue, progressEndColor]
        }
    }

    @IBInspectable
    var progressEndColor: UIColor = .whiteSmoke {
        willSet {
            shapeLayer.progressPathGradientColors = [progressStartColor, newValue]
        }
    }

    var isAnimating: Bool {
        return animationCompletionTimer?.isValid ?? false
    }

    var currentValue: ProgressValue? {
        return isAnimating ? layer.presentation()?.value(forKey: .value) as? ProgressValue : value
    }

    weak var delegate: ProgressViewDelegate?

    private var initialValue: CGFloat = 0
    private var animationCompletionTimer: Timer?
    private var onAnimationCompletion: ProgressAnimationCompletion?

    private var progressGradientColors: [UIColor] = [.pastelGray, .whiteSmoke] {
        didSet {
            shapeLayer.progressPathGradientColors = progressGradientColors
        }
    }

    private var shapeLayer: CircularProgressShapeLayer {
        guard let drawLayer = layer as? CircularProgressShapeLayer else {
            fatalError("\(#function) - Wrong designable view configuration!")
        }

        return drawLayer
    }

    // Initialiation

    override init(frame: CGRect) {
        super.init(frame: frame)

        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setup()
    }
}

// MARK: - ProgressView Lifecycle

extension ProgressView {

    override class var layerClass: AnyClass {
        return CircularProgressShapeLayer.self
    }

    override func prepareForInterfaceBuilder() {
        setup()
    }

}

// MARK: - ProgressView API

extension ProgressView {

    func animateProgress(from fromValue: ProgressValue = 0, to value: ProgressValue, duration: ProgressAnimationDuration = 0, completion: ProgressAnimationCompletion? = nil) {
        if isAnimating {
            self.value = currentValue ?? value
            shapeLayer.removeAnimation(forKey: .value)
        }

        shapeLayer.speed = 1
        shapeLayer.beginTime = 0
        shapeLayer.timeOffset = 0
        shapeLayer.animationDuration = duration

        onAnimationCompletion = completion

        animationCompletionTimer?.invalidate()
        animationCompletionTimer = nil
        animationCompletionTimer = Timer.scheduledTimer(timeInterval: duration, target: self, selector: #selector(onAnimationTimerDidComplete(_:)), userInfo: onAnimationCompletion, repeats: false)

        self.initialValue = fromValue
        self.value = value
    }

}

// MARK: - ProgressView Helpers

private extension ProgressView {

    func setup() {
        shapeLayer.backgroundColor = UIColor.clear.cgColor

        shapeLayer.value = value
        shapeLayer.minValue = minValue
        shapeLayer.maxValue = maxValue

        shapeLayer.basePathColor = pathColor
        shapeLayer.progressPathGradientColors = progressGradientColors

        shapeLayer.progressViewDelegate = self
    }

    func didUpdateProgress(newValue: ProgressValue) {
        delegate?.didAnimateProgress(to: newValue, on: self)
    }

    func completeAnimation(withTimer timer: Timer) {
        delegate?.didProgressComplete(on: self)

        guard let onCompleteAnimation = timer.userInfo as? ProgressAnimationCompletion else {
            return
        }

        onCompleteAnimation()
    }

    @objc func onAnimationTimerDidComplete(_ sender: Timer) {
        completeAnimation(withTimer: sender)
    }

}

// MARK: - CircularProgressShapeLayer

private class CircularProgressShapeLayer: CAShapeLayer {
    @NSManaged var value: CGFloat
    @NSManaged var minValue: CGFloat
    @NSManaged var maxValue: CGFloat

    @NSManaged var basePathColor: UIColor?
    @NSManaged var progressPathGradientColors: [UIColor]

    var initialValue: CGFloat = 0
    var animationDuration: TimeInterval = 1.0
    var animationTimingFunction: CAMediaTimingFunctionName = .easeInEaseOut

    weak var progressViewDelegate: ProgressView?

    private let startAngle: CGFloat = 0
    private let endAngle: CGFloat = .pi * 2
    private let basePathWidth: CGFloat = 2
    private let progressPathWidth: CGFloat = 6
    private let pathCapStyle: CGLineCap = .round
    private let progressPathGradientColorLocations: [CGFloat] = [0, 1]

    private static let ValueAnimationKey = "value"
}

// MARK: - CircularProgressShapeLayer Lifecycle

private extension CircularProgressShapeLayer {

    override class func needsDisplay(forKey key: String) -> Bool {
        guard key == ValueAnimationKey else {
            return super.needsDisplay(forKey: key)
        }

        return true
    }

    override func draw(in ctx: CGContext) {
        super.draw(in: ctx)

        UIGraphicsPushContext(ctx)

        drawBasePath()
        drawProgressPath(in: ctx)

        if let updatedValue = value(forKey: CircularProgressShapeLayer.ValueAnimationKey) as? CGFloat {
            progressViewDelegate?.didUpdateProgress(newValue: updatedValue)
        }

        UIGraphicsPopContext()
    }

    override func action(forKey event: String) -> CAAction? {
        guard event == CircularProgressShapeLayer.ValueAnimationKey else {
            return super.action(forKey: event)
        }

        let animation = CABasicAnimation(keyPath: CircularProgressShapeLayer.ValueAnimationKey)
        animation.duration = animationDuration
        animation.fromValue = initialValue
        animation.timingFunction = CAMediaTimingFunction(name: animationTimingFunction)

        return animation
    }

}

// MARK: - CircularProgressShapeLayer Helpers

private extension CircularProgressShapeLayer {

    func drawBasePath() {
        guard let pathColor = basePathColor else {
            return
        }

        let center = CGPoint(x: bounds.midX, y: bounds.midY)
        let offset = max(basePathWidth, progressPathWidth) / 2
        let radius = min(bounds.width, bounds.height) / 2 - offset
        let path = UIBezierPath(arcCenter: center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        path.lineWidth = basePathWidth
        path.lineCapStyle = pathCapStyle

        pathColor.setStroke()
        path.stroke()
    }

    func drawProgressPath(in ctx: CGContext) {
        guard value > 0, !progressPathGradientColors.isEmpty else {
            return
        }

        let twoPi = CGFloat.pi * 2
        let center = CGPoint(x: bounds.midX, y: bounds.midY)
        let offSet = progressPathWidth / 2 + (progressPathWidth * 1.5)
        let radius = min(bounds.width, bounds.height) / 2 - offSet
        let fromAngle = twoPi - (CGFloat.pi / 2)
        let toAngle = ((value - minValue) / (maxValue - minValue) * twoPi) + fromAngle
        let path = UIBezierPath(arcCenter: center, radius: radius, startAngle: fromAngle, endAngle: toAngle, clockwise: true)

        ctx.setLineWidth(progressPathWidth)
        ctx.setLineCap(pathCapStyle)
        ctx.setLineJoin(.round)

        let cgColors = progressPathGradientColors.map { $0.cgColor }
        let strokeColor = cgColors.first ?? UIColor.slateGray.cgColor

        if let pathGradient = CGGradient(colorsSpace: nil, colors: cgColors as CFArray, locations: progressPathGradientColorLocations) {
            let gradientStart = CGPoint(x: bounds.midX, y: bounds.minY) // Top
            let gradientEnd = CGPoint(x: bounds.midX, y: bounds.maxY)   // Bottom

            ctx.saveGState()
            ctx.addPath(path.cgPath)
            ctx.replacePathWithStrokedPath()
            ctx.clip()
            ctx.drawLinearGradient(pathGradient, start: gradientStart, end: gradientEnd, options: .drawsBeforeStartLocation)
            ctx.restoreGState()
        } else {
            ctx.setStrokeColor(strokeColor)
            ctx.addPath(path.cgPath)
            ctx.drawPath(using: .stroke)
        }
    }

}

// MARK: - AnimationKeys

private enum AnimationKeys: String {
    case value
}

// MARK: - CALayer

private extension CALayer {

    func removeAnimation(forKey key: AnimationKeys) {
        removeAnimation(forKey: key.rawValue)
    }

    func animation(forKey key: AnimationKeys) -> CAAnimation? {
        return animation(forKey: key.rawValue)
    }

    func value(forKey key: AnimationKeys) -> Any? {
        return value(forKey: key.rawValue)
    }

}
