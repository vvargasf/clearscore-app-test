//
//  AppConfig.swift
//  ClearScoreAppTest
//
//  Created by Victor Vargas Fariñas on 16/01/2020.
//  Copyright © 2020 NUX Factory. All rights reserved.
//

import Foundation

// MARK: - AppConfig

struct AppConfig {
    // We could use .xcconfig for production-ready apps.
    
    static let enabledUITestingArgumentKey = "enabled-ui-testing"
    static let apiRootURL = URL(string: "https://5lfoiyb0b3.execute-api.us-west-2.amazonaws.com/prod")!
}

// MARK: - AppConfig Helpers

extension AppConfig {

    static var isTestingEnv: Bool {
        return UserDefaults.standard.bool(forKey: AppConfig.enabledUITestingArgumentKey)
    }

}
