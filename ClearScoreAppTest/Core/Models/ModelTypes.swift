//
//  ModelTypes.swift
//  ClearScoreAppTest
//
//  Created by Victor Vargas Fariñas on 29/12/2019.
//  Copyright © 2019 NUX Factory. All rights reserved.
//

import Foundation

// MARK: - FetchableValue

struct FetchableValue<T> {
    indirect enum RemoteValue<T> {
        case notFetched
        case fetched(value: T)
    }

    let url: URL

    var value: RemoteValue<T>
    var fetchedValue: T? {
        if case let .fetched(value) = value {
            return value
        }

        return nil
    }

    // Helpers

    mutating func update(newValue: T) {
        value = .fetched(value: newValue)
    }
}

// MARK: - FetchableValue: Codable

extension FetchableValue: Codable {

    // Decodable

    init(from decoder: Decoder) throws {
        let template = try decoder.singleValueContainer().decode(String.self)

        guard let url = URL(string: template) else {
            throw DecodingError.dataCorrupted(DecodingError.Context(codingPath: [], debugDescription: ""))
        }

        self.url = url
        value = .notFetched
    }

    // Encodable

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()

        try container.encode(url)
    }

}
