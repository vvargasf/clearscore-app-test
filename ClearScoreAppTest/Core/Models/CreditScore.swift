//
//  CreditScore.swift
//  ClearScoreAppTest
//
//  Created by Victor Vargas Fariñas on 02/01/2020.
//  Copyright © 2020 NUX Factory. All rights reserved.
//

import Foundation

// MARK: - DashboardStatusType

enum DashboardStatusType: String {
    case pass = "PASS"
    case unknown
}

// MARK: - DashboardStatusType: Codable

extension DashboardStatusType: Codable {

    init(from decoder: Decoder) throws {
        let typeValue = try decoder.singleValueContainer().decode(String.self)

        self = DashboardStatusType(rawValue: typeValue) ?? .unknown
    }

}

// MARK: - PersonaType

enum PersonaType: String {
    case inexperienced = "INEXPERIENCED"
    case unknown
}

// MARK: - PersonaType: Codable

extension PersonaType: Codable {

    init(from decoder: Decoder) throws {
        let typeValue = try decoder.singleValueContainer().decode(String.self)

        self = PersonaType(rawValue: typeValue) ?? .unknown
    }

}

// MARK: - CreditStatusType

enum CreditStatusType: String {
    case match = "MATCH"
    case unknown
}

// MARK: - CreditStatusType: Codable

extension CreditStatusType: Codable {

    init(from decoder: Decoder) throws {
        let typeValue = try decoder.singleValueContainer().decode(String.self)

        self = CreditStatusType(rawValue: typeValue) ?? .unknown
    }

}

// MARK: - CreditScore

struct CreditScore {
    let dashboardStatus: DashboardStatusType
    let personaType: PersonaType
    let coachingSummary: CoachingSummary
    let creditInfo: ReportInfo
}

// MARK: - CreditScore: Codable

extension CreditScore: Codable {

    enum CodingKeys: String, CodingKey {
        case dashboardStatus
        case personaType
        case coachingSummary
        case creditInfo = "creditReportInfo"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        dashboardStatus = try container.decode(DashboardStatusType.self, forKey: .dashboardStatus)
        personaType = try container.decode(PersonaType.self, forKey: .personaType)
        coachingSummary = try container.decode(CoachingSummary.self, forKey: .coachingSummary)
        creditInfo = try container.decode(ReportInfo.self, forKey: .creditInfo)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(dashboardStatus, forKey: .dashboardStatus)
        try container.encode(personaType, forKey: .personaType)
        try container.encode(coachingSummary, forKey: .coachingSummary)
        try container.encode(creditInfo, forKey: .creditInfo)
    }

}

// MARK: - CreditScore.ReportInfo

extension CreditScore {

    struct ReportInfo {
        let client: String
        let minScoreValue: Int
        let maxScoreValue: Int
        let scoreValue: Int
        let status: CreditStatusType
    }

}

// MARK: - CreditScore.ReportInfo: Codable

extension CreditScore.ReportInfo: Codable {

    enum CodingKeys: String, CodingKey {
        case client = "clientRef"
        case minScoreValue
        case maxScoreValue
        case scoreValue = "score"
        case status
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        client = try container.decode(String.self, forKey: .client)
        minScoreValue = try container.decode(Int.self, forKey: .minScoreValue)
        maxScoreValue = try container.decode(Int.self, forKey: .maxScoreValue)
        scoreValue = try container.decode(Int.self, forKey: .scoreValue)
        status = try container.decode(CreditStatusType.self, forKey: .status)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(client, forKey: .client)
        try container.encode(minScoreValue, forKey: .minScoreValue)
        try container.encode(maxScoreValue, forKey: .maxScoreValue)
        try container.encode(scoreValue, forKey: .scoreValue)
        try container.encode(status, forKey: .status)
    }

}

// MARK: - CreditScore.CoachingSummary

extension CreditScore {

    struct CoachingSummary {
        let activeToDo: Bool
        let activeChat: Bool
        let selected: Bool
        let numberOfToDoItems: Int
        let numberOfCompletedToDoItems: Int
    }

}

// MARK: - CreditScore.CoachingSummary: Codable

extension CreditScore.CoachingSummary: Codable {

    enum CodingKeys: String, CodingKey {
        case activeToDo = "activeTodo"
        case activeChat
        case selected
        case numberOfToDoItems = "numberOfTodoItems"
        case numberOfCompletedToDoItems = "numberOfCompletedTodoItems"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        activeToDo = try container.decode(Bool.self, forKey: .activeToDo)
        activeChat = try container.decode(Bool.self, forKey: .activeChat)
        selected = try container.decode(Bool.self, forKey: .selected)
        numberOfToDoItems = try container.decode(Int.self, forKey: .numberOfToDoItems)
        numberOfCompletedToDoItems = try container.decode(Int.self, forKey: .numberOfCompletedToDoItems)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(activeToDo, forKey: .activeToDo)
        try container.encode(activeChat, forKey: .activeChat)
        try container.encode(selected, forKey: .selected)
        try container.encode(numberOfToDoItems, forKey: .numberOfToDoItems)
        try container.encode(numberOfCompletedToDoItems, forKey: .numberOfCompletedToDoItems)
    }

}
