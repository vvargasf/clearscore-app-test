//
//  NetworkController.swift
//  ClearScoreAppTest
//
//  Created by Victor Vargas Fariñas on 29/12/2019.
//  Copyright © 2019 NUX Factory. All rights reserved.
//

import Foundation

typealias DataCompletionHandler = (Data?, URLResponse?, Error?) -> Void

// MARK: - NetworkController

class NetworkController {
    private let id = UUID()
    private let session: URLSession
    private let randomIDs = 0 ..< Int.max

    private let concurrentQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.qualityOfService = .userInitiated

        return queue
    }()

    private var requests: [String: AnyObject] = [:]

    // Initialization

    init() {
        session = .defaultSession()
    }
}

// MARK: - NetworkController Helpers

extension NetworkController {

    func fetchValue<V: Codable>(for url: URL, withCompletion completion: @escaping (Result<V, NetworkError>) -> Void) {
        let fetchRequest = FetchRequest<V>(url: url, session: session)
        let requestId = "\(url.absoluteString)_\(Int.random(in: randomIDs))"

        requests[requestId] = fetchRequest
        fetchRequest.execute { [weak self] result in
            self?.requests[requestId] = nil
            completion(result)
        }
    }
    
}

// MARK: - SimplifiedDataTask

class SimplifiedDataTask: URLSessionDataTask {
    private let url: URL
    private let completion: DataCompletionHandler
    private let session: URLSession = .defaultSession()

    // Initialization

    init(url: URL, completion: @escaping DataCompletionHandler) {
        self.url = url
        self.completion = completion
    }

    // Lifecycle

    override func resume() {
        if let data = try? Data(contentsOf: url) {
            completion(data, HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil), nil)
        } else {
            completion(nil, nil, nil) // An error could be returned but for this case and simplicity it's not necessary.
        }
    }
}
