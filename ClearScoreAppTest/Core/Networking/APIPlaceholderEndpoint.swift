//
//  APIPlaceholderEndpoint.swift
//  ClearScoreAppTest
//
//  Created by Victor Vargas Fariñas on 29/12/2019.
//  Copyright © 2019 NUX Factory. All rights reserved.
//

import Foundation

struct APIPlaceholderEndpoint {
    // Credit Score Info
    
    static var creditScoreURL: URL {
        guard !AppConfig.isTestingEnv else {
            return URL(fileURLWithPath: Bundle.main.path(forResource: "CreditScore", ofType: "json")!)
        }

        return AppConfig.apiRootURL.appendingPathComponent("mockcredit/values")
    }
}
