//
//  NetworkTypes.swift
//  ClearScoreAppTest
//
//  Created by Victor Vargas Fariñas on 29/12/2019.
//  Copyright © 2019 NUX Factory. All rights reserved.
//

import Foundation

// MARK: - NetworkError

enum NetworkError: String {
    case temporary = "network.request.error.temporary"
    case unreachable = "network.request.error.unreachable"
    case unrecoverable = "networkrequest.error.unrecoverable"
}

// MARK: - NetworkError: LocalizedError

extension NetworkError: LocalizedError {

    var errorDescription: String? {
        return NSLocalizedString(rawValue + ".title", tableName: "NetworkErrors", comment: "The title for a network error.")
    }

    var recoverySuggestion: String? {
        return NSLocalizedString(rawValue + ".recovery", tableName: "NetworkErrors", comment: "The description for a network error.")
    }

}

// MARK: - Error

extension Error {

    var asNetworkError: NetworkError {
        switch self {
        case URLError.timedOut, URLError.cannotConnectToHost, URLError.networkConnectionLost, URLError.notConnectedToInternet:
            return NetworkError.unreachable

        case URLError.resourceUnavailable:
            return NetworkError.temporary

        default:
            return NetworkError.unrecoverable
        }
    }

}

// MARK: - HTTPURLResponse

extension HTTPURLResponse {

    func validate() throws {
        switch statusCode {
        case 400, 402, 403, 405 ..< 500: throw URLError(.badServerResponse)
        case 500 ..< 600: throw URLError(.resourceUnavailable)
        case -1001: throw URLError(.timedOut)
        case -1004: throw URLError(.cannotConnectToHost)
        case -1005: throw URLError(.networkConnectionLost)
        case -1009: throw URLError(.notConnectedToInternet)
        default: ()
        }
    }

}
