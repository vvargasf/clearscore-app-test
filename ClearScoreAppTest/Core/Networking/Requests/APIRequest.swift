//
//  APIRequest.swift
//  ClearScoreAppTest
//
//  Created by Victor Vargas Fariñas on 29/12/2019.
//  Copyright © 2019 NUX Factory. All rights reserved.
//

import Foundation

// MARK: - FetchRequest

class FetchRequest<ModelType: Decodable>: JSONDataRequest {
    let url: URL
    let session: URLSession

    var task: URLSessionDataTask?

    // Initialization

    init(url: URL, session: URLSession) {
        self.url = url
        self.session = session
    }
}
