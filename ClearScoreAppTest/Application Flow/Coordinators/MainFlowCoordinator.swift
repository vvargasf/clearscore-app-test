//
//  MainFlowCoordinator.swift
//  ClearScoreAppTest
//
//  Created by Victor Vargas Fariñas on 02/01/2020.
//  Copyright © 2020 NUX Factory. All rights reserved.
//

import UIKit

// MARK: - MainFlowCoordinator

class MainFlowCoordinator: NSObject {
    let mainViewController: MainViewController

    // Initialization

    init(mainViewController: MainViewController) {
        self.mainViewController = mainViewController

        super.init()

        configure(viewController: mainViewController)
    }
}

// MARK: - MainFlowCoordinator: Coordinator

extension MainFlowCoordinator: Coordinator {

    func configure(viewController: UIViewController) {
        (viewController as? MainCoordinated)?.mainCoordinator = self
        (viewController as? Networked)?.networkController = NetworkController()

        guard let navigationController = viewController as? UINavigationController, let rootViewController = navigationController.viewControllers.first else {
            return
        }

        configure(viewController: rootViewController)
    }

}
