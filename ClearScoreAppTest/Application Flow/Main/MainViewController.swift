//
//  MainViewController.swift
//  ClearScoreAppTest
//
//  Created by Victor Vargas Fariñas on 27/12/2019.
//  Copyright © 2019 NUX Factory. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, MainCoordinated, ItemViewController {
    @IBOutlet private weak var scoreProgressView: CreditScoreView!

    var networkController: NetworkController?

    private(set) var item: FetchableValue<CreditScore>?
    private(set) var error: Error?

    weak var mainCoordinator: MainFlowCoordinator?
}

// MARK: - Lifecycle

extension MainViewController {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        setupProgressView()
        fetchData()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        setupStatusBar()
    }

}

// MARK: - Workflow

extension MainViewController {

    func handle(result: CreditScore) {
        item?.update(newValue: result)
        reloadData()
    }

    func handle(error: Error) {
        self.error = error

        // Handle error flow.
    }

}

// MARK: - MainViewController Helpers

private extension MainViewController {

    func setup() {
        view.backgroundColor = .whiteSmoke
        navigationItem.title = "Dashboard"

        item = FetchableValue<CreditScore>(url: APIPlaceholderEndpoint.creditScoreURL, value: .notFetched)
    }

    func setupStatusBar() {
        navigationController?.navigationBar.barStyle = .black
    }

    func setupProgressView() {
        scoreProgressView.viewModel = CreditScoreView.ViewModel()
    }

    func reloadData() {
        guard let creditScore = item?.fetchedValue else {
            return
        }

        scoreProgressView.viewModel = CreditScoreView.ViewModel(with: creditScore)
    }

}

