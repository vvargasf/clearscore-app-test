//
//  CreditScoreView.swift
//  ClearScoreAppTest
//
//  Created by Victor Vargas Fariñas on 05/01/2020.
//  Copyright © 2020 NUX Factory. All rights reserved.
//

import UIKit

class CreditScoreView: UIView {
    @IBOutlet private weak var progressView: ProgressView!
    @IBOutlet private weak var scoreTitleLabel: UILabel!
    @IBOutlet private weak var scoreValueLabel: UILabel!
    @IBOutlet private weak var scoreSubtitleLabel: UILabel!

    var viewModel = ViewModel() {
        didSet {
            guard viewModel.isDataValid else {
                return
            }

            refreshScore()
        }
    }

    // Initialization

    override init(frame: CGRect) {
        super.init(frame: frame)

        loadFromNib()
        setup()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        loadFromNib()
        setup()
    }
}

// MARK: - CreditScoreView Helpers

private extension CreditScoreView {

    func loadFromNib() {
        let nibName = NSStringFromClass(type(of: self)).components(separatedBy: ".").last! as String
        let bundle = Bundle(for: type(of: self))

        guard bundle.path(forResource: nibName, ofType: "nib") != nil else {
            return
        }

        let customNib = UINib(nibName: nibName, bundle: bundle)
        let customView: UIView = customNib.instantiate(withOwner: self, options: nil).first as! UIView
        customView.frame = bounds
        customView.backgroundColor = .clear
        customView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        addSubview(customView)
    }

    func setup() {
        accessibilityIdentifier = "creditScoreView"
        backgroundColor = .clear

        progressView.backgroundColor = .clear
        progressView.pathColor = .licorice
        progressView.progressStartColor = .tigerEye
        progressView.progressEndColor = .gargoyleGas

        progressView.minValue = 0
        progressView.maxValue = 100
        progressView.value = 0

        progressView.delegate = self

        scoreValueLabel.accessibilityIdentifier = "scoreValueLabel"
        scoreValueLabel.text = " "

        scoreSubtitleLabel.accessibilityIdentifier = "scoreSubtitleLabel"
        scoreSubtitleLabel.text = "Loading score..."
    }

    func refreshScore() {
        scoreValueLabel.text = "\(viewModel.scoreValue)"
        scoreSubtitleLabel.text = "out of \(viewModel.scoreMaxValue)"

        let lastScoreValue = progressView.currentValue ?? ProgressView.ProgressValue(viewModel.scoreMinValue)

        progressView.minValue = ProgressView.ProgressValue(viewModel.scoreMinValue)
        progressView.maxValue = ProgressView.ProgressValue(viewModel.scoreMaxValue)
        progressView.animateProgress(from: lastScoreValue, to: ProgressView.ProgressValue(viewModel.scoreValue), duration: 1, completion: nil)
    }

}

// MARK: - CreditScoreView.ViweModel

extension CreditScoreView {

    struct ViewModel {
        var scoreValue = 0
        var scoreMinValue = 0
        var scoreMaxValue = 0

        var isDataValid: Bool {
            return scoreMinValue < scoreMaxValue && scoreMaxValue > 0
        }
    }

}

// MARK: - CreditScoreView.ViweModel Initialization

extension CreditScoreView.ViewModel {

    init(with scoreInfo: CreditScore) {
        scoreValue = scoreInfo.creditInfo.scoreValue
        scoreMinValue = scoreInfo.creditInfo.minScoreValue
        scoreMaxValue = scoreInfo.creditInfo.maxScoreValue
    }

}

// MARK: - CreditScoreView ProgressViewDelegate

extension CreditScoreView: ProgressViewDelegate {

    func didAnimateProgress(to value: ProgressView.ProgressValue, on view: ProgressView) {
        scoreValueLabel.text = "\(Int(value))"
    }

    func didProgressComplete(on view: ProgressView) {
        // Nothing to do.
    }

}
