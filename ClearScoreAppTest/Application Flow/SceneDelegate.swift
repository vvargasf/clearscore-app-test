//
//  SceneDelegate.swift
//  ClearScoreAppTest
//
//  Created by Victor Vargas Fariñas on 27/12/2019.
//  Copyright © 2019 NUX Factory. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var coordinator: MainFlowCoordinator?

    // MARK: - UIWindowSceneDelegate

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else {
            return
        }

        UINavigationBar.setCustomAppearance()

        guard let initialViewController = mainViewController(from: window?.rootViewController) else {
            return
        }

        if CommandLine.arguments.contains(AppConfig.enabledUITestingArgumentKey) {
            reset()
            UserDefaults.standard.set(true, forKey: AppConfig.enabledUITestingArgumentKey)
        } else {
            UserDefaults.standard.removeObject(forKey: AppConfig.enabledUITestingArgumentKey)
        }

        coordinator = MainFlowCoordinator(mainViewController: initialViewController)
    }

}

// MARK: - SceneDelegate Helpers

private extension SceneDelegate {

    func mainViewController(from root: UIViewController?) -> MainViewController? {
        guard root != nil else {
            return nil
        }

        if let main = root as? MainViewController {
            return main
        }

        if let navigationController = root as? UINavigationController {
            return mainViewController(from: navigationController.viewControllers.first)
        }

        return nil
    }

    func reset() {
        guard let defaults = Bundle.main.bundleIdentifier else {
            return
        }

        UserDefaults.standard.removePersistentDomain(forName: defaults)
    }

}

