//
//  Helpers.swift
//  ClearScoreAppTestTests
//
//  Created by Victor Vargas Fariñas on 04/01/2020.
//  Copyright © 2020 NUX Factory. All rights reserved.
//

import XCTest
@testable import ClearScoreAppTest

extension XCTestCase {

    func verify<T>(url: String, in fetchableValue: FetchableValue<T>, file: StaticString = #file, line: UInt = #line) {
        XCTAssertEqual(url, fetchableValue.url.absoluteString, file: file, line: line)

        if case .fetched(_) = fetchableValue.value {
            XCTFail("A decoded FetchableValue should have an unfetched value.", file: file, line: line)
        }
    }

    func jsonDataFromFile<Model: Decodable>(withName name: String) -> Model? {
        let bundle = Bundle(for: type(of: self))

        guard let url = bundle.url(forResource: name, withExtension: "json"), let data = try? Data(contentsOf: url) else {
            XCTFail("Could not load the data from the .json file.")

            return nil
        }

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .iso8601

        guard let model = try? decoder.decode(Model.self, from: data) else {
            XCTFail("Could not decode the JSON data..")

            return nil
        }

        return model
    }

}
