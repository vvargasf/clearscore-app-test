//
//  NetworkRequestTests.swift
//  ClearScoreAppTestTests
//
//  Created by Victor Vargas Fariñas on 04/01/2020.
//  Copyright © 2020 NUX Factory. All rights reserved.
//

import XCTest
@testable import ClearScoreAppTest

class NetworkRequestTests: XCTestCase {

    // Tests
    
    func testRequestExecution() {
        let session = PartialMockSession()
        let request = MockRequest(session: session)
        let asyncExpectation = expectation(description: "Request async block executed...")

        request.execute { _ in
            XCTAssertEqual(request.data, PartialMockDataTask.dummyData)
            XCTAssertEqual(request.response, PartialMockDataTask.dummyResponse)

            asyncExpectation.fulfill()
        }

        waitForExpectations(timeout: 1, handler: nil)
    }
}

// MARK: - MockRequest

class MockRequest: NetworkRequest {
    typealias ModelType = Int

    let session: URLSession
    let url = URL(string: "csapptest.com")!

    var task: URLSessionDataTask?
    var data: Data?
    var response: URLResponse?

    var urlRequest: URLRequest {
        return URLRequest(url: url)
    }

    // Initialization

    init(session: URLSession) {
        self.session = session
    }

    // NetworkRequest

    func deserialize(_ data: Data?, response: HTTPURLResponse) throws -> Int {
        self.data = data
        self.response = response

        return 0
    }
}

// MARK: - PartialMockSession

class PartialMockSession: URLSession {

    // Lkifecycle

    override func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        return PartialMockDataTask(completionHandler: completionHandler)
    }
}

// MARK: - PartialMockDataTask

class PartialMockDataTask: URLSessionDataTask {
    static let dummyData = Data()
    static let dummyResponse = HTTPURLResponse(url: URL(string: "csapptest.com")!, statusCode: 200, httpVersion: nil, headerFields: nil)

    let completionHandler: (Data?, URLResponse?, Error?) -> Void

    // Initialization

    init(completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        self.completionHandler = completionHandler
    }

    // Lkifecycle

    override func resume() {
        completionHandler(PartialMockDataTask.dummyData, PartialMockDataTask.dummyResponse, nil)
    }
}
