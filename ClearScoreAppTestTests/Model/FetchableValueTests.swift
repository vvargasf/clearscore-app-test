//
//  FetchableValueTests.swift
//  ClearScoreAppTestTests
//
//  Created by Victor Vargas Fariñas on 04/01/2020.
//  Copyright © 2020 NUX Factory. All rights reserved.
//

import XCTest
@testable import ClearScoreAppTest

class FetchableValueTests: XCTestCase {

    // Tests

    func testDataCorruptedError() {
        let json = "{ \"avatar_url\": \"\" }".data(using: .utf8)!

        XCTAssertThrowsError(try JSONDecoder().decode(TestModel.self, from: json))
    }

    func testDataDecodedSuccess() {
        let url = "https://api.adorable.io/avatars/200/app+test@clouscore.com"
        let json = "{ \"avatar_url\": \"\(url)\" }".data(using: .utf8)!
        let testModel = try? JSONDecoder().decode(TestModel.self, from: json)

        XCTAssertNotNil(testModel)
        verify(url: url, in: testModel!.testProperty)
    }
}

// MARK: - FetchableValueTests.TestModel

private extension FetchableValueTests {

    struct TestModel: Decodable {
        let testProperty: FetchableValue<Bool>

        // Decodable

        enum CodingKeys: String, CodingKey {
            case avatarURL = "avatar_url"
        }

        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)

            testProperty = try container.decode(FetchableValue.self, forKey: .avatarURL)
        }
    }

}
