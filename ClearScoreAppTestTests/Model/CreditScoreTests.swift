//
//  CreditScoreTests.swift
//  ClearScoreAppTestTests
//
//  Created by Victor Vargas Fariñas on 04/01/2020.
//  Copyright © 2020 NUX Factory. All rights reserved.
//

import XCTest
@testable import ClearScoreAppTest

class CreditScoreTests: XCTestCase {    

    // Tests

    func testDecoding() {
        guard let creditScore: CreditScore = jsonDataFromFile(withName: "CreditScore") else {
            XCTFail("Invalid .json data file.")

            return
        }

        XCTAssertEqual(creditScore.creditInfo.scoreValue, 327)
        XCTAssertTrue(creditScore.creditInfo.status == .match)
    }
}
