//
//  XCUIApplication+Main.swift
//  ClearScoreAppTestUITests
//
//  Created by Victor Vargas Fariñas on 15/01/2020.
//  Copyright © 2020 NUX Factory. All rights reserved.
//

import XCTest

extension XCUIApplication {

    var isMainReady: Bool {
        otherElements["creditScoreView"].waitForExistence(timeout: 1)
    }

}
