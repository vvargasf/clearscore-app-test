//
//  ClearScoreAppTestUITests.swift
//  ClearScoreAppTestUITests
//
//  Created by Victor Vargas Fariñas on 14/01/2020.
//  Copyright © 2020 NUX Factory. All rights reserved.
//

import XCTest

class ClearScoreAppTestUITests: XCTestCase {
    var app: XCUIApplication!

    // MARK: - XCTestCase

    override func setUp() {
        continueAfterFailure = false

        app = XCUIApplication()
        app.launchArguments.append("enabled-ui-testing")
    }
}

// MARK: - ClearScoreAppTestUITests Tests

extension ClearScoreAppTestUITests {

    func testInitialAppStateIsValid() {
        app.launch()

        XCTAssertTrue(app.navigationBars["Dashboard"].waitForExistence(timeout: 1))
        XCTAssertTrue(app.isMainReady)
    }

    func testCreditScoreInfoIsValid() {
        app.launch()

        let creditScoreView = app.otherElements["creditScoreView"].firstMatch
        XCTAssertTrue(creditScoreView.waitForExistence(timeout: 1))

        let creditScoreInfoLabels = creditScoreView.descendants(matching: .staticText)
        let scoreValueLabel = creditScoreInfoLabels.element(matching: .staticText, identifier: "scoreValueLabel")
        let scoreSubtitleLabel = creditScoreInfoLabels.element(matching: .staticText, identifier: "scoreSubtitleLabel")

        XCTAssertTrue(scoreValueLabel.waitForExistence(timeout: 1))
        XCTAssertTrue(scoreSubtitleLabel.waitForExistence(timeout: 1))
        XCTAssertEqual(scoreValueLabel.label, "255")
        XCTAssertTrue(scoreSubtitleLabel.label.contains("600"))
    }

}
